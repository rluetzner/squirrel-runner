extends KinematicBody2D


export var move_speed = 200.0
export var jump_speed = 100.0
export var fall_speed = 50.0


var _x_direction = 0
var _y_speed = 0.0
var _jumping = false
var _jump_direction = 0.0
onready var _sprite = $AnimatedSprite


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_inputs()


func get_inputs():
	_x_direction = 0
	if Input.is_action_pressed("move_left"):
		_x_direction -= 1
	if Input.is_action_pressed("move_right"):
		_x_direction += 1
	if Input.is_action_pressed("jump") && !_jumping:
		_jumping = true
		_jump_direction = _x_direction
		_y_speed -= jump_speed
	update_sprite()


func update_sprite():
	if _jumping:
		_sprite.stop()
		_sprite.frame = 3
		return		
	if _x_direction < 0:
		_sprite.flip_h = false
		_sprite.play()
	elif _x_direction > 0:
		_sprite.flip_h = true
		_sprite.play()
	else:
		_sprite.stop()
		_sprite.frame = 0


func _physics_process(delta):
	if _jumping:
		_y_speed += fall_speed * delta
	var reduced_move_speed = move_speed / 3 if _jumping else move_speed
	var actual_move_speed = move_speed if _x_direction == _jump_direction else reduced_move_speed
	var move = Vector2(
		_x_direction * actual_move_speed,
		_y_speed)
	move_and_slide(move)
